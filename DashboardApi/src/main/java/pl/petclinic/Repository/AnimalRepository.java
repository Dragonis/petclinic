package pl.petclinic.Repository;


import org.springframework.data.repository.CrudRepository;
import pl.petclinic.Dao.AnimalDao;

public interface AnimalRepository extends CrudRepository<AnimalDao, Integer> {

}
