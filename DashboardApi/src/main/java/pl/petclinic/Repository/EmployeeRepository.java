package pl.petclinic.Repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.petclinic.Dao.EmployeeDao;

@Repository
public interface EmployeeRepository extends CrudRepository<EmployeeDao, Integer> {

}
