package pl.petclinic.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.petclinic.Dao.AnimalDao;
import pl.petclinic.Service.AnimalService;

@RestController
@RequestMapping("/api/")
public class ApiController {
    @Autowired
    public AnimalService service;
    public class User {
        public String name;
        public String surname;
    }

    @GetMapping("/")
    public User index() {
        User user = new User();
        user.name = "Test";
        return user;
    }

//    @GetMapping("/animal/")
//    public ResponseEntity<Iterable<AnimalDao>> animal() {
//        return ResponseEntity.ok(service.findAll());
//    }
}
