import groovyx.net.http.RESTClient
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Shared
import spock.lang.Specification

import java.net.http.HttpRequest

//@SpringBootTest
class JsonPlaceholderTest extends Specification{

    def "test /todos/{id}"() {
        given:
        def url = 'https://jsonplaceholder.typicode.com'
        def path = "/todos/" + id

        RESTClient client = new RESTClient(url)
        def response = client.get(path: path)
        then:
        expect:
        response.status == 200  // sprawdź czy odpowiedź zwraca status OK (200)

        where:
        id | title | completed
         1 | 'delectus aut autem' | false
         2 | 'quis ut nam facilis et officia qui' | false
         10 | 'illo est ratione doloremque quia maiores aut' | true
    }


}
