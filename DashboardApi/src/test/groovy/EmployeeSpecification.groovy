import groovyx.net.http.RESTClient
import spock.lang.Specification;

class EmployeeSpecification extends Specification {

    def "test /api/employee/"() {
        given:
        def url = 'http://localhost:80'
        def path = "/api/employee/"

        RESTClient client = new RESTClient(url)
        def response = client.get(path: path)
        then:
        expect:
        response.status == 200  // sprawdź czy odpowiedź zwraca status OK (200)

        where:
        id | firstname | lastname | depertament | numberphone | email
        1 | 'Jan' | 'Kowalski' | 'Dział księgowy' | '48 739 854 621' | 'jan.kowalski@logo.pl'
        2 | 'Anna' | 'Nowak' | 'Dział sprzedaży' | '15 822 64 79' | 'anna.nowak@logo.pl'
    }
}
