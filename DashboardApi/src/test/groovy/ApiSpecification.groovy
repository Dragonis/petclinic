import groovyx.net.http.RESTClient;
import spock.lang.Specification;

class ApiSpecification extends Specification {

    def "test /api/"() {
        given:
        def url = 'http://localhost:80'
        def path = "/api/"

        RESTClient client = new RESTClient(url)
        def response = client.get(path: path)
        then:
        expect:
        response.status == 200  // sprawdź czy odpowiedź zwraca status OK (200)
        response.data.name == 'Test'
        response.data.surname == null
    }

}
