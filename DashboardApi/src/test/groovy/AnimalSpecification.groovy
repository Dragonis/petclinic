import groovyx.net.http.RESTClient
import spock.lang.Specification;

class AnimalSpecification extends Specification {

    def "test /api/animal/"() {
        given:
        def url = 'http://localhost:80'
        def path = "/api/animal/"

        RESTClient client = new RESTClient(url)
        def response = client.get(path: path)
        then:
        expect:
        response.status == 200  // sprawdź czy odpowiedź zwraca status OK (200)

        where:
        id | ksywka | rasa
        1 | 'Pimpek' | 'Labrador'
        2 | 'Ksywka' | 'Rasa'
    }

}
