
-- CREATE TABLE users (
--    username varchar(255),
--    email varchar(255),
--    password varchar(255)
-- );
--
-- CREATE TABLE roles (
--    name varchar(255)
-- );

--INSERT INTO users(id, email, username, password) VALUES (1,'test@test.pl','aaa','$2a$10$HUg/AHBJv1.GQcNEuyXA.ecj7eYyzsyjnv9iRljVmeq9B7KaHL1cS');
--INSERT INTO user_roles(user_id, role_id) VALUES (1,1);

-- It's OK
INSERT INTO roles(name) VALUES('ROLE_USER');
INSERT INTO roles(name) VALUES('ROLE_MODERATOR');
INSERT INTO roles(name) VALUES('ROLE_ADMIN');
