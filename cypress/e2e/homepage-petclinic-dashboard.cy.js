describe('localhost', () => {

   beforeEach(()=>{

   })

   it("Open dashboard", function () {
      cy.visit('localhost')
   })

   it("Should be visible application name on 'Homepage'", function () {
       cy.visit('localhost')
       cy.get('.example-app-name').contains('PetClinic').should('be.visible')
       cy.screenshot('animals-company')
   });
})

describe('localhost/employee/add', () => {

   it("Save employee to database", function () {
      cy.visit('localhost')

   //Visit 'Lekarze' page"
     cy.contains('Lekarze').click()
     cy.url().should('include', '/employee')
     cy.screenshot('lekarze')

   //Should click 'Dodaj lekarza button"
     cy.get('.border > .btn').click()

   // Fill doctor dialog modal
     cy.get('#firstname').type("test")
     cy.get('#lastname').type("test")
     cy.get('#departament').type("test")
     cy.get('#email').type("test")
     cy.get('#numberphone').type("test")

     // Click save button
     cy.get('.border > .btn-info').click()
   })
})


describe('localhost/employee/file ', () => {

   it("Should download file", function () {

      cy.visit('localhost')
      cy.contains('Lekarze').click()
      cy.url().should('include', '/employee')
      cy.screenshot('lekarze')

      //Should choose raport in customselect
      cy.get(':nth-child(3) > .cdk-column-pdf > form.ng-untouched > div > .custom-select').select('Umowa o prace')

      //Should download 'umowa o prace.pdf' file"
      //click download link
      cy.get(':nth-child(3) > .cdk-column-pdf > form.ng-valid > div > .btn').click()

      // check umowa o prace.pdf file is downloaded
      cy.readFile('cypress//Downloads//Umowa o prace.pdf').should('exist')

})

describe('localhost/animal', () => {

    it("Check animal website exists", function () {
       cy.visit('localhost')
       cy.contains('Zwierzątka').click()
       cy.url().should('include', '/animal')
       cy.screenshot('Zwierzątka')
    })
})

describe('localhost/animal/add', () => {

   it("Should add animal to database", function () {

       cy.visit('localhost')
       cy.contains('Zwierzątka').click()
       cy.url().should('include', '/animal')
       cy.screenshot('Zwierzątka')

       cy.get('.border > .btn').click()

       //Should fill Zwierzątka DialogModal
       cy.get('#ksywka').type("test")
       cy.get('#rodzaj').type("test")
       cy.get('#rasa').type("test")
       cy.get('#dataPrzyjecia').type("test")
       cy.get('#opis').type("test")

       //Should save data from Zwierzątka DialogModal"
       cy.get('.border > .btn-info').click()
})

it("Should download animal file", function () {

  cy.visit('localhost')
  cy.contains('Zwierzątka').click()
  cy.url().should('include', '/animal')
  cy.screenshot('Zwierzątka')

  //Should choose DIagnoza raport
  //click choose raport customselect
  cy.get(':nth-child(3) > .cdk-column-pdf > form.ng-untouched > div > .custom-select').select('Diagnoza')


  //Should download Diagnoza.pdf file
  cy.get(':nth-child(3) > .cdk-column-pdf > form.ng-valid > div > .btn').click()

  //Check Diagnoza.pdf file is downloaded
  cy.readFile('cypress//Downloads//Diagnoza.pdf').should('exist')

  })
})
})