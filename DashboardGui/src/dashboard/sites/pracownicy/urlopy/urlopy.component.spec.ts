import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UrlopyComponent } from './urlopy.component';

describe('UrlopyComponent', () => {
  let component: UrlopyComponent;
  let fixture: ComponentFixture<UrlopyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UrlopyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UrlopyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
