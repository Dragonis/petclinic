import {ChangeDetectorRef, Component, HostListener, Injectable, OnInit, ViewChild} from '@angular/core';
import {HttpClient, HttpClientModule, HttpHeaders, HttpResponse} from "@angular/common/http";
import {ModalDismissReasons, NgbDropdown, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {UntypedFormBuilder, UntypedFormGroup, NgForm, Validators} from "@angular/forms";
import {Employee} from "./Employee";
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from "@angular/material/sort";
import * as fileSaver from 'file-saver';
import { FileSaverService } from 'ngx-filesaver';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-friend',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  public employees: Employee[];
  private clickedEmployeeRowInTable: Employee;
  public httpClient: HttpClient;
  private modalService: NgbModal;
  private closeResult: String;
  addEmployeeForm: UntypedFormGroup;
  editForm: UntypedFormGroup;
  reportsForm: UntypedFormGroup;
  fb: UntypedFormBuilder;
  private deleteId: number;

  private url_home: string = environment.apiUrl+"employee/";
  private url: string = environment.apiUrl+"employee/report/";

  isDisabledDownloadButon = true;

  ELEMENT_DATA: Employee[];
  dataSource: MatTableDataSource<Employee>;

  //Columns names, table data from datasource, pagination and sorting
  displayedColumns: string[] = ['firstname', 'lastname', 'department', 'numberphone', 'email', 'Action Buttons', 'pdf', 'summary'];

  // data for table dropdown
  reportDownloadUrlPath = [
    { url: "http://localhost/api/employee/report/umowa_o_prace", filename: "Umowa o prace" },
    { url: "http://localhost/api/employee/report/wypowiedzenie", filename: "Wypowiedzenie" },
    { url: "http://localhost/api/employee/report/test", filename: "Test" },
  ];

  constructor(httpClient: HttpClient, modalService: NgbModal, fb: UntypedFormBuilder,
              private _FileSaverService: FileSaverService) {
    this.httpClient = httpClient;
    this.modalService = modalService;
    this.fb = fb;
  }

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  ngOnInit(): void {

    this.getMatTableEmployees();

    this.dataSource = new MatTableDataSource<Employee>(this.ELEMENT_DATA);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    this.addEmployeeForm = this.fb.group({

      firstname: ['', [Validators.required, Validators.minLength(3)]],
      lastname: ['', [Validators.required, Validators.minLength(3)]],
      numberphone: ['', [Validators.required, Validators.minLength(3)]],
    });

    this.editForm = this.fb.group({

      id: [''],
      firstname: [''],
      lastname: [''],
      department: [''],
      email: [''],
      numberphone: [''],

      /*      contractDate: [''],
            nameCompanyOwner: [''],
            surnameCompanyOwner: [''],
            homeAdress: [''],
            startWork: [''],
            endWork: [''],
            companyName: [''],
            dateDocumentSigned: [''],
            kindOfWork: [''],
            placeOfWork: [''],
            workingHours: [''],
            salary: [''],
            dateToStartWork: [''],*/
    });

    this.reportsForm = this.fb.group({
      dropdownValue: [null]
    });
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  onSubmit(form: NgForm) {
    const url = this.url_home + 'addnew';
    this.httpClient.post(url, form.value)
      .subscribe((result) => {
        this.ngOnInit(); //reload the table
      });
    this.modalService.dismissAll(); //dismiss the modal
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  openDetails(targetModal, employee: Employee) {

    this.modalService.open(targetModal, {
      centered: true,
      backdrop: 'static',
      size: 'lg'
    });
    console.log("debug inserted data by getElementById in Szczegóły button: " + JSON.stringify(employee));
    document.getElementById('fname').setAttribute('value', employee.firstname);
    document.getElementById('lname').setAttribute('value', employee.lastname);
    document.getElementById('dept').setAttribute('value', employee.department);
    document.getElementById('email2').setAttribute('value', employee.email);
    document.getElementById('cntry').setAttribute('value', employee.numberphone);

    /*  document.getElementById('fcontractDate').setAttribute('value', employee.contractDate);
      document.getElementById('fnameCompanyOwner').setAttribute('value', employee.nameCompanyOwner);
      document.getElementById('fsurnameCompanyOwner').setAttribute('value', employee.surnameCompanyOwner);
      document.getElementById('fhomeAdress').setAttribute('value', employee.homeAdress);
      document.getElementById('fstartWork').setAttribute('value', employee.startWork);
      document.getElementById('fendWork').setAttribute('value', employee.endWork);
      document.getElementById('fcompanyName').setAttribute('value', employee.companyName);
      document.getElementById('fdateDocumentSigned').setAttribute('value', employee.dateDocumentSigned);
      document.getElementById('fkindOfWork').setAttribute('value', employee.kindOfWork);
      document.getElementById('fplaceOfWork').setAttribute('value', employee.placeOfWork);
      document.getElementById('fworkingHours').setAttribute('value', employee.workingHours);
      document.getElementById('fsalary').setAttribute('value', employee.salary);
      document.getElementById('fdateToStartWork').setAttribute('value', employee.dateToStartWork);
  */
  }

  openEdit(targetModal, employee: Employee) {
    this.modalService.open(targetModal, {
      backdrop: 'static',
      size: 'lg'
    });
    this.editForm.patchValue({
      id: employee.id,
      firstname: employee.firstname,
      lastname: employee.lastname,
      department: employee.department,
      numberphone: employee.numberphone,
      email: employee.email,

      /*      contractDate: employee.contractDate,
            nameCompanyOwner: employee.nameCompanyOwner,
            surnameCompanyOwner: employee.surnameCompanyOwner,
            homeAdress: employee.homeAdress,
            startWork: employee.startWork,
            endWork: employee.endWork,
            companyName: employee.companyName,
            dateDocumentSigned: employee.dateDocumentSigned,
            kindOfWork: employee.kindOfWork,
            placeOfWork: employee.placeOfWork,
            workingHours: employee.workingHours,
            salary: employee.salary,
            dateToStartWork: employee.dateToStartWork*/
    });
  }

  onSave() {
    const editURL = this.url_home + this.editForm.value.id + '/edit';
    console.log("Edit form values: " + JSON.stringify(this.editForm.value));
    this.httpClient.put(editURL, this.editForm.value)
      .subscribe((results) => {
        this.ngOnInit();
        this.modalService.dismissAll();
      });
  }

  openDelete(targetModal, employee: Employee) {
    this.deleteId = employee.id;
    this.modalService.open(targetModal, {
      backdrop: 'static',
      size: 'lg'
    });
  }

  onDelete() {
    const deleteURL = this.url_home + this.deleteId + '/delete';
    this.httpClient.delete(deleteURL)
      .subscribe((results) => {
        this.ngOnInit();
        this.modalService.dismissAll();
      });
  }

  getMatTableEmployees() {
    let response = this.httpClient.get(this.url_home);
    response.subscribe(report => this.dataSource.data = report as Employee[]);

  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  onTableRowClicked(row){
    console.log('Clicked table row: Firstname: ' + row.firstname + ", Lastname: " + row.lastname);
    this.clickedEmployeeRowInTable = row;
  }

  downloadPdf(pdfUrl: string, pdfName: string, employee: Employee) {

    console.log("Downloaded file: " + "PdfName: "+ JSON.stringify(pdfName) +", Employee data: " + JSON.stringify(employee));
    this.httpClient.post(this.url+pdfName, employee, { responseType: 'blob' as 'json' })
      .subscribe((result: any) => {
        fileSaver.saveAs(result, pdfName);
      });
  }

  downloadReportFromDropdown(employee: Employee){
    this.downloadPdf(this.url, this.reportsForm.value.dropdownValue, employee);
  }

  showLogdOnChooseOptionInDropdown(): void {
    const index = JSON.stringify(this.clickedEmployeeRowInTable.id);
    const name = JSON.stringify(this.reportsForm.value);
    console.log('Table dropdown value (reportsForm): ' + index);
    console.log('Table dropdown name (reportsForm): ' + name);

    //this.reportsForm.get('ButtonNr').setValue(name);
  }

}

