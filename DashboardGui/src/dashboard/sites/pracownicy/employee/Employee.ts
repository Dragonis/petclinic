export class Employee {
  id: number;
  firstname: string;
  lastname: string;
  department: string;
  numberphone: string;
  email: string;
}
