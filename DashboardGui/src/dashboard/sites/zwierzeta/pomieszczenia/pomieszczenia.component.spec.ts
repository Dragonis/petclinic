import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PomieszczeniaComponent } from './pomieszczenia.component';

describe('PomieszczeniaComponent', () => {
  let component: PomieszczeniaComponent;
  let fixture: ComponentFixture<PomieszczeniaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PomieszczeniaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PomieszczeniaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
