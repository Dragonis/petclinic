import {ChangeDetectorRef, Component, HostListener, Injectable, OnInit, ViewChild} from '@angular/core';
import {HttpClient, HttpClientModule, HttpHeaders, HttpResponse} from "@angular/common/http";
import {ModalDismissReasons, NgbDropdown, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {UntypedFormBuilder, UntypedFormGroup, NgForm} from "@angular/forms";
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from "@angular/material/sort";
import * as fileSaver from 'file-saver';
import { FileSaverService } from 'ngx-filesaver';
import {Animal} from './Animal'

@Component({
  selector: 'zwierzeta',
  templateUrl: './zwierzeta.component.html',
  styleUrls: ['./zwierzeta.component.css']
})
export class ZwierzetaComponent implements OnInit {


  public animals: Animal[];
  private clickedanimalRowInTable: Animal;
  public httpClient: HttpClient;
  private modalService: NgbModal;
  private closeResult: String;
  private editForm: UntypedFormGroup;
  private reportsForm: UntypedFormGroup;
  private fb: UntypedFormBuilder;
  private deleteId: number;

  private url_home: string = "http://localhost/api/animal/";
  private url: string = "http://localhost/api/animal/report/";

  isDisabledDownloadButon = true;

  ELEMENT_DATA: Animal[];
  dataSource: MatTableDataSource<Animal>;

  //Columns names, table data from datasource, pagination and sorting
  displayedColumns: string[] = ['ksywka', 'rodzaj', 'rasa', 'dataPrzyjecia', 'opis', 'Action Buttons', 'pdf', 'summary'];

  // data for table dropdown
  reportDownloadUrlPath = [
    { url: "http://localhost/api/animal/report/diagnoza", filename: "Diagnoza" },
    { url: "http://localhost/api/animal/report/recepta", filename: "Recepta" },
    { url: "http://localhost/api/animal/report/test", filename: "Test" },
  ];

  constructor(httpClient: HttpClient, modalService: NgbModal, fb: UntypedFormBuilder,
              private _FileSaverService: FileSaverService) {
    this.httpClient = httpClient;
    this.modalService = modalService;
    this.fb = fb;
  }

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  ngOnInit() {

    this.dataSource = new MatTableDataSource<Animal>(this.ELEMENT_DATA);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    this.editForm = this.fb.group({

      id: [''],
      ksywka: [''],
      rodzaj: [''],
      rasa: [''],
      dataPrzyjecia: [''],
      opis: [''],

    });

    this.reportsForm = this.fb.group({
      dropdownValue: [null]
    });

    this.getMatTableWithAnimals();
  }


  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  onSubmit(form: NgForm) {
    const url = this.url_home + '/addnew';
    this.httpClient.post(url, form.value)
      .subscribe((result) => {
        this.ngOnInit(); //reload the table
      });
    this.modalService.dismissAll(); //dismiss the modal
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  openDetails(targetModal, animal: Animal) {
    this.modalService.open(targetModal, {
      centered: true,
      backdrop: 'static',
      size: 'lg'
    });
    console.log("debug inserted data by getElementById in Szczegóły button: " + JSON.stringify(animal));
    document.getElementById('fksywka').setAttribute('value', animal.ksywka);
    document.getElementById('frodzaj').setAttribute('value', animal.rodzaj);
    document.getElementById('frasa').setAttribute('value', animal.rasa);
    document.getElementById('fdataPrzyjecia').setAttribute('value', animal.dataPrzyjecia);
    document.getElementById('fopis').setAttribute('value', animal.opis);

  }

  openEdit(targetModal, animal: Animal) {
    this.modalService.open(targetModal, {
      backdrop: 'static',
      size: 'lg'
    });
    this.editForm.patchValue({
      id: animal.id,
      ksywka: animal.ksywka,
      rodzaj: animal.rodzaj,
      rasa: animal.rasa,
      dataPrzyjecia: animal.dataPrzyjecia,
      opis: animal.opis,

    });
  }

  onSave() {
    const editURL = this.url_home + '/' + this.editForm.value.id + '/edit/';
    console.log("Edit form values: " + JSON.stringify(this.editForm.value));
    this.httpClient.put(editURL, this.editForm.value)
      .subscribe((results) => {
        this.ngOnInit();
        this.modalService.dismissAll();
      });
  }

  openDelete(targetModal, animal: Animal) {
    this.deleteId = animal.id;
    this.modalService.open(targetModal, {
      backdrop: 'static',
      size: 'lg'
    });
  }

  onDelete() {
    const deleteURL = this.url_home + '/' + this.deleteId + '/delete/';
    this.httpClient.delete(deleteURL)
      .subscribe((results) => {
        this.ngOnInit();
        this.modalService.dismissAll();
      });
  }

  getMatTableWithAnimals() {
    let response = this.httpClient.get(this.url_home);
    response.subscribe(report => this.dataSource.data = report as Animal[]);

  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  onTableRowClicked(row){
    this.clickedanimalRowInTable = row;
  }

  downloadPdf(pdfUrl: string, pdfName: string, animal: Animal) {

    console.log("Downloaded file: " + "PdfName: "+ JSON.stringify(pdfName) +", animal data: " + JSON.stringify(animal));
    this.httpClient.post(this.url+pdfName, animal, { responseType: 'blob' as 'json' })
      .subscribe((result: any) => {
        fileSaver.saveAs(result, pdfName);
      });
  }

  downloadReportFromDropdown(animal: Animal){
    this.downloadPdf(this.url, this.reportsForm.value.dropdownValue, animal);
  }

  showLogdOnChooseOptionInDropdown() {
    console.log("Table dropdown value (reportsForm): " + JSON.stringify(this.reportsForm.value));
  }

}
