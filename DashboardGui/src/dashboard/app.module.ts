import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { DashboardComponent } from './dashboard.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HomeComponent } from './sites/home/home.component';
import { EmployeeComponent } from './sites/pracownicy/employee/employee.component';
import { HttpClientModule } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import {MatInputModule} from "@angular/material/input";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatButtonModule} from "@angular/material/button";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatListModule} from "@angular/material/list";
import {MatIconModule} from "@angular/material/icon";
import { EcpComponent } from './sites/pracownicy/ecp/ecp.component';
import { GrafikComponent } from './sites/pracownicy/grafik/grafik.component';
import { UrlopyComponent } from './sites/pracownicy/urlopy/urlopy.component';
import { DelegacjeComponent } from './sites/pracownicy/delegacje/delegacje.component';
import { CatComponent } from './components/cat/cat.component';
import { DogComponent } from './components/dog/dog.component';
import { ZwierzetaComponent } from './sites/zwierzeta/zwierzeta/zwierzeta.component';
import { PomieszczeniaComponent } from './sites/zwierzeta/pomieszczenia/pomieszczenia.component';
import { ZabiegiComponent } from './sites/zwierzeta/zabiegi/zabiegi.component';
import { LekiComponent } from './sites/zwierzeta/leki/leki.component';

@NgModule({
    declarations: [
        DashboardComponent,
        HomeComponent,
        EmployeeComponent,
        EcpComponent,
        GrafikComponent,
        UrlopyComponent,
        DelegacjeComponent,
        CatComponent,
        DogComponent,
        ZwierzetaComponent,
        PomieszczeniaComponent,
        ZabiegiComponent,
        LekiComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        NgbModule,
        HttpClientModule,
        FormsModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        MatTableModule,
        MatDialogModule,
        MatPaginatorModule,
        MatSortModule,
        MatInputModule,
        MatFormFieldModule,
        MatToolbarModule,
        MatSidenavModule,
        MatListModule,
        MatButtonModule,
        MatIconModule,

    ],
    providers: [],
    exports: [
        DashboardComponent
    ],
    bootstrap: [DashboardComponent]
})
export class AppModule { }
