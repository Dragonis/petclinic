import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from "./sites/home/home.component";
import {EmployeeComponent} from "./sites/pracownicy/employee/employee.component";
import {EcpComponent} from "./sites/pracownicy/ecp/ecp.component";
import {GrafikComponent} from "./sites/pracownicy/grafik/grafik.component";
import {UrlopyComponent} from "./sites/pracownicy/urlopy/urlopy.component";
import {DelegacjeComponent} from "./sites/pracownicy/delegacje/delegacje.component";
import {ZwierzetaComponent} from "./sites/zwierzeta/zwierzeta/zwierzeta.component";
import {PomieszczeniaComponent} from "./sites/zwierzeta/pomieszczenia/pomieszczenia.component";
import {LekiComponent} from "./sites/zwierzeta/leki/leki.component";
import {ZabiegiComponent} from "./sites/zwierzeta/zabiegi/zabiegi.component";

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'home', component: HomeComponent},
  {path: 'employee', component: EmployeeComponent},
  {path: 'ecp', component: EcpComponent},
  {path: 'grafik', component: GrafikComponent},
  {path: 'urlopy', component: UrlopyComponent},
  {path: 'delegacje', component: DelegacjeComponent},

  {path: 'animal', component: ZwierzetaComponent},
  {path: 'leki', component: LekiComponent},
  {path: 'zabiegi', component: ZabiegiComponent},
  {path: 'pomieszczenia', component: PomieszczeniaComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
