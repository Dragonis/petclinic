import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ZabiegiComponent } from './zabiegi.component';

describe('ZabiegiComponent', () => {
  let component: ZabiegiComponent;
  let fixture: ComponentFixture<ZabiegiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ZabiegiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ZabiegiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
