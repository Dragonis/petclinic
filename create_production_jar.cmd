@echo

:: install nodejs
:: npm install @angular/cli -g
:: npm install pnpm -g
:: npm install cypress -g
:: Make 2 environment variable:
::   Add a JAVA_HOME variable equal to something like: C:\Java\JDK1.7.0_25_x64 :: to use mvnw to install packages
::   Add %JAVA_HOME%\bin to the beginning of your PATH variable. :: to use command: java -jar file.jar
::   Download maven and add maven path to system PATH variable :: To use mvn package

call mvn package

robocopy ./DashboardApi/target/ ./production *.jar
robocopy ./DashboardApi/reports/ ./production/reports /e
robocopy ./RegisterApi/target/ ./production *.jar

cd ./production

start java -jar DashboardApi-1.0-SNAPSHOT.jar
start java -jar RegisterApi-2.7.0.jar

:: cli e2e tests
cd ./..
start cypress run
